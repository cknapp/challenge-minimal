import { snakeCase } from 'lodash';
import {
  InventoryClient,
  inventoryUpdate,
  RecordWithWMS,
  SkuBatchData,
  SkuBatchToSkuId,
  skuBatchUpdate,
  WMSWarehouseMeta,
} from './interfaces.util';
import {
  appData,
  appSkuBatchData,
  appSkuBatchDataForSkuBatchIds,
  skuBatchIdsFromAppDb,
  skuBatchIdsFromInventoryDb,
  warehouseData
} from "./db/data";
import { HttpInventoryClient } from './httpInventoryClient';

const logger = console;

/**
 * Create a list of records for a skuBatch record that maps skuBatchId + warehouseId
 * @param skuBatchRecord
 */
const makeWarehouseRecordsForSkuBatchRecord = (skuBatchRecord: SkuBatchToSkuId): RecordWithWMS[] => {
  return warehouseData.map(
    (warehouse: WMSWarehouseMeta): RecordWithWMS => ({
        skuBatchId: skuBatchRecord.skuBatchId,
        skuId: skuBatchRecord.skuId,
        wmsId: skuBatchRecord.wmsId,
        quantityPerUnitOfMeasure: skuBatchRecord.quantityPerUnitOfMeasure ?? 1,
        isArchived: skuBatchRecord.isArchived,
        isDeleted: skuBatchRecord.isDeleted,
        warehouseId: warehouse.warehouseId,
      }),
  );
};

const inventoryClient: InventoryClient = new HttpInventoryClient();

/**
 * Converts a list of skuBatchIds from the app db into an insert to inventory.
 * @param skuBatchIdsToInsert
 */
export async function skuBatchToInserts(skuBatchIdsToInsert: string[]): Promise<RecordWithWMS[]> {
  const badSkuBatchCounter = { count: 0 };

  // create our inserts
  const inserts: RecordWithWMS[] = skuBatchIdsToInsert
    .reduce((arr: RecordWithWMS[], skuBatchId: string): RecordWithWMS[] => {
      const skuBatchRecordFromAppDb: SkuBatchToSkuId | undefined = appData.find(
        (skuBatchToSkuId: SkuBatchToSkuId): boolean => skuBatchToSkuId.skuBatchId == skuBatchId,
      );

      if (!skuBatchRecordFromAppDb) {
        logger.error(`no records found in app SkuBatch [skuBatchId=${skuBatchId}}]`);
        badSkuBatchCounter.count += 1;
        return arr;
      }

      arr.push(...makeWarehouseRecordsForSkuBatchRecord(skuBatchRecordFromAppDb));
      return arr;
    }, []);

  logger.log(`created inserts [count=${inserts.length}, badSkuBatchRecordCount=${badSkuBatchCounter.count}]`);

  return inserts;
}

/**
 * Diffs the inventory between app SkuBatch and inventory to determine
 * what we need to copy over.
 */
export async function getDeltas(): Promise<string[]> {
  try {
    const inventorySkuBatchIds: Set<string> = new Set<string>(skuBatchIdsFromInventoryDb
        .map((r: { skuBatchId: string }) => r.skuBatchId));
    return [...new Set<string>(skuBatchIdsFromAppDb.map((r: { id: string }) => r.id))]
        .filter((x: string) => !inventorySkuBatchIds.has(x));
  } catch (err) {
    logger.error('error querying databases for skuBatchIds');
    logger.error(err);
    throw err;
  }
}

/**
 * Finds the deltas between two lists of SkuBatchData
 * @param appSkuBatchData
 * @param inventorySkuBatchData
 */
export const findDeltas = (
    appSkuBatchData: SkuBatchData[],
    inventorySkuBatchData: SkuBatchData[],
): SkuBatchData[] => {
  logger.log('finding data changes between inventory and app SkuBatch datasets');

  return appSkuBatchData
    .filter((appSbd: SkuBatchData) => {
      const inventoryRecord: SkuBatchData | undefined = inventorySkuBatchData
          .find((r: SkuBatchData): boolean => r.skuBatchId == appSbd.skuBatchId);

      if (!inventoryRecord) {
        // if we cannot find the matching record, we have a problem
        logger.warn(`cannot find matching inventory record! [skuBatchId=${appSbd.skuBatchId}]`);
        // instead of throwing an error, return false to prevent an update on this record
        return false;
      }

      // go through each key and see if it is different, if so, mark this record as needing updated
      return Object.entries(inventoryRecord)
        .filter(([k, _]) => !['skuBatchId'].includes(k))
        .some(([key, inventoryValue]) => {
          const appValue = appSbd[key as keyof typeof appSbd];
          return (key == 'skuId' && inventoryValue == null) || (key != 'skuId' && inventoryValue != appValue);
        });
    })
};

/**
 * Finds changes in data between the app SkuBatch+Sku and inventory tables
 */
export async function findChangesBetweenDatasets(): Promise<SkuBatchToSkuId[]> {
  logger.log('finding app SkuBatch data that has changed and <> the inventory data');

  const updates: SkuBatchData[] = await [appSkuBatchData].reduce(
    async (accumPromise: Promise<SkuBatchData[]>, inventorySkuBatchData: SkuBatchData[]) => {
      const accum: SkuBatchData[] = await accumPromise;
      const skuBatchIds: string[] = inventorySkuBatchData.map((sbd: SkuBatchData) => sbd.skuBatchId);

      logger.log(`querying Logistics.SkuBatch for data [skuBatchIdCount=${skuBatchIds.length}]`);
      // fetch SkuBatch+Sku data from the app database
      const appSkuBatchData: SkuBatchData[] = appSkuBatchDataForSkuBatchIds;

      // if we have a count mismatch, something is wrong, and we should log out a warning
      if (appSkuBatchData.length != inventorySkuBatchData.length) {
        // I had this logic flipped based on the comment but the data didn't support that happening, so I switched it around
        // implement the logic to log a message with the IDs missing from app
        // data that exist in the inventory data
        const appIds = appSkuBatchData.map(appDataRecord => appDataRecord.skuBatchId);
        const inventoryIds = inventorySkuBatchData.map(inventoryDataRecord => inventoryDataRecord.skuBatchId);
        const missingIds = appIds.filter(id => !inventoryIds.includes(id));
        logger.warn(`found inventory items that don't exist in the app data: ${missingIds.join(', ')}`);
      }

      // push our new inventory updates into the accumulator list
      const ds: SkuBatchData[] = findDeltas(appSkuBatchData, inventorySkuBatchData);

      accum.push(...ds);
      return accum;
    },
    Promise.resolve([] as SkuBatchData[]),
  );

  logger.log(`built updates [count=${updates.length}]`);

  return updates.map(record => ({
    skuBatchId: record.skuBatchId,
    skuId: record.skuId!,
    quantityPerUnitOfMeasure: record.quantityPerUnitOfMeasure,
    wmsId: Number(record.wmsId!),
    isArchived: record.isArchived,
    isDeleted: record.isDeleted,
  }));
}

/**
 * Updates inventory data from app SkuBatch and Sku
 */
export async function copyMissingInventoryRecordsFromSkuBatch(): Promise<void | Error> {
  logger.log('copying missing inventory records from app Sku/SkuBatch');

  // find out what skuBatchIds don't exist in inventory
  const skuBatchIdsToInsert: string[] = await getDeltas();
  logger.log(`copying new skuBatch records... [skuBatchCount=${skuBatchIdsToInsert.length}]`);
  const inserts = await skuBatchToInserts(skuBatchIdsToInsert);
  const attempts = inserts.map(record => inventoryClient.create(record));
  const successes = attempts.filter(record => record).length;
  const failures = attempts.filter(record => !record).length;
  logger.log(`inserted new records with ${successes} successful inserts and ${failures} failures`);

  logger.log('done updating additive data to inventory from app db');
}

/**
 * Pulls inventory and SkuBatch data and finds changes in SkuBatch data
 * that are not in the inventory data.
 */
export async function updateInventoryDeltasFromSkuBatch(): Promise<void> {
  logger.log('updating inventory from deltas in "SkuBatch" data');

  try {
    const recordsToUpdate: SkuBatchToSkuId[] = await findChangesBetweenDatasets();
    // There was no warehouse data available for these in the data that came back from the "db".
    // I assume there would be WH data in the actual records in order to push to an external API
    // or a way to look it up using wms id as a linking property ?  I'm not sure where it would come from.
    // It would probably need to be looked up/included early in the process to be able to determine if it is
    // a delta or not.
    // I did this to at least get some warehouse data into the payload for updates but would not do this in reality
    const attempts = recordsToUpdate.flatMap(makeWarehouseRecordsForSkuBatchRecord)
      .map(record => inventoryClient.update(record));
    const successes = attempts.filter(record => record).length;
    const failures = attempts.filter(record => !record).length;

    logger.log(`updated records with ${successes} successful updates and ${failures} failures`);
  } catch (err) {
    logger.error(err);
    throw err;
  }

  logger.log('done updating inventory from deltas from app db');
}

/**
 * Primary entry point to sync SkuBatch data from the app
 * database over to the inventory database
 */
export async function sync(): Promise<void | Error> {
  try {
    await copyMissingInventoryRecordsFromSkuBatch();
    await updateInventoryDeltasFromSkuBatch();
  } catch (err) {
    logger.error('error syncing skuBatch data');
    return Promise.reject(err);
  }
}