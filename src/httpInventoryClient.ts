import { InventoryClient, RecordWithWMS, SkuBatchData } from './interfaces.util';
import axios, { AxiosResponse } from 'axios';

const BASE_URL = 'https://local-inventory.nabis.dev/v1';
const INVENTORY_ENDPOINT = `${BASE_URL}/inventory`;
const INVENTORY_AGGREGATE_ENDPOINT = `${BASE_URL}/inventory-aggregate`;

export class HttpInventoryClient implements InventoryClient {
  // the diffing to be done would have to happen in the API at this point
  // The contract for the API is somewhat unclear. I'm not entirely sure what payload it expects, so I'm sending the whole record
  // create will POST the whole record to be sure it gets created
  create = async (data: RecordWithWMS) => {
    return await this._call(axios.post, data);
  };

  // update will PUT the whole record to be sure it is replaced with the correct representation
  update = async (data: RecordWithWMS) => {
    return await this._call(axios.put, data);
  };

  _call = async (call: any, data: RecordWithWMS) => {
    const inventoryCall = call(INVENTORY_ENDPOINT, data);
    const aggregateCall = call(INVENTORY_AGGREGATE_ENDPOINT, data);

    const results: AxiosResponse[] = await Promise.all([inventoryCall, aggregateCall]);

    return !results.some(result => result.status != 200);
  }
}