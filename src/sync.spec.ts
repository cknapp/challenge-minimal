import { findChangesBetweenDatasets, findDeltas, getDeltas, skuBatchToInserts, } from './sync';
import { SkuBatchData } from "./interfaces.util";

describe('sync', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('.skuBatchToInserts', () => {
    it('should return a list of records to insert', async () => {
      const data = [
        {
          skuBatchId: 'sku-batch-id-1',
          skuId: 'sku-id-1',
          quantityPerUnitOfMeasure: 25,
        },
        {
          skuBatchId: 'sku-batch-id-2',
          skuId: 'sku-id-1',
          quantityPerUnitOfMeasure: 25,
        },
        {
          skuBatchId: 'sku-batch-id-3',
          skuId: 'sku-id-2',
          quantityPerUnitOfMeasure: 1,
        },
      ];

      await expect(
        skuBatchToInserts(
          data.map((d) => d.skuBatchId),
        ),
      ).resolves.toStrictEqual([
        {
          skuBatchId: 'sku-batch-id-1',
          skuId: 'sku-id-1',
          wmsId: 1234,
          quantityPerUnitOfMeasure: 1,
          isArchived: false,
          isDeleted: false,
          warehouseId: 'warehouse-1'
        },
        {
          skuBatchId: 'sku-batch-id-1',
          skuId: 'sku-id-1',
          wmsId: 1234,
          quantityPerUnitOfMeasure: 1,
          isArchived: false,
          isDeleted: false,
          warehouseId: 'warehouse-2'
        },
        {
          skuBatchId: 'sku-batch-id-1',
          skuId: 'sku-id-1',
          wmsId: 1234,
          quantityPerUnitOfMeasure: 1,
          isArchived: false,
          isDeleted: false,
          warehouseId: 'warehouse-3'
        },
        {
          skuBatchId: 'sku-batch-id-1',
          skuId: 'sku-id-1',
          wmsId: 1234,
          quantityPerUnitOfMeasure: 1,
          isArchived: false,
          isDeleted: false,
          warehouseId: 'warehouse-4'
        },
        {
          skuBatchId: 'sku-batch-id-2',
          skuId: 'sku-id-1',
          wmsId: 1235,
          quantityPerUnitOfMeasure: 1,
          isArchived: false,
          isDeleted: false,
          warehouseId: 'warehouse-1'
        },
        {
          skuBatchId: 'sku-batch-id-2',
          skuId: 'sku-id-1',
          wmsId: 1235,
          quantityPerUnitOfMeasure: 1,
          isArchived: false,
          isDeleted: false,
          warehouseId: 'warehouse-2'
        },
        {
          skuBatchId: 'sku-batch-id-2',
          skuId: 'sku-id-1',
          wmsId: 1235,
          quantityPerUnitOfMeasure: 1,
          isArchived: false,
          isDeleted: false,
          warehouseId: 'warehouse-3'
        },
        {
          skuBatchId: 'sku-batch-id-2',
          skuId: 'sku-id-1',
          wmsId: 1235,
          quantityPerUnitOfMeasure: 1,
          isArchived: false,
          isDeleted: false,
          warehouseId: 'warehouse-4'
        },
        {
          skuBatchId: 'sku-batch-id-3',
          skuId: 'sku-id-1',
          wmsId: 1236,
          quantityPerUnitOfMeasure: 1,
          isArchived: false,
          isDeleted: false,
          warehouseId: 'warehouse-1'
        },
        {
          skuBatchId: 'sku-batch-id-3',
          skuId: 'sku-id-1',
          wmsId: 1236,
          quantityPerUnitOfMeasure: 1,
          isArchived: false,
          isDeleted: false,
          warehouseId: 'warehouse-2'
        },
        {
          skuBatchId: 'sku-batch-id-3',
          skuId: 'sku-id-1',
          wmsId: 1236,
          quantityPerUnitOfMeasure: 1,
          isArchived: false,
          isDeleted: false,
          warehouseId: 'warehouse-3'
        },
        {
          skuBatchId: 'sku-batch-id-3',
          skuId: 'sku-id-1',
          wmsId: 1236,
          quantityPerUnitOfMeasure: 1,
          isArchived: false,
          isDeleted: false,
          warehouseId: 'warehouse-4'
        }
      ]);
    });
  });

  describe('.getDeltas', () => {
    it('should find deltas', async () => {
      // example of the data below:
      // skuBatchIds = ['sku-batch-id-1', 'sku-batch-id-2', 'sku-batch-id-3', 'sku-batch-id-4'];
      // appSkuBatchIds = [...skuBatchIds, 'sku-batch-id-5', 'sku-batch-id-6']; // 5 and 6 are new
      await expect(getDeltas()).resolves.toStrictEqual(['sku-batch-id-5', 'sku-batch-id-6']);
    });
  });

  describe('.findDelta', () => {
    it('should pick up changes to quantityPerUnitOfMeasure', async () => {
      const appData = [{
        skuBatchId: '1',
        skuId: '1',
        wmsId: '1',
        quantityPerUnitOfMeasure: 5,
        isArchived: false,
        isDeleted: false,
      }];

      const inventoryData = [{
        skuBatchId: '1',
        skuId: '1',
        wmsId: '1',
        quantityPerUnitOfMeasure: 10,
        isArchived: false,
        isDeleted: false,
      }];

      const deltas: SkuBatchData[] = findDeltas(appData, inventoryData);
      expect(deltas.length).toBe(1);
      expect(deltas[0]).toEqual({
        skuBatchId: '1',
        skuId: '1',
        wmsId: '1',
        quantityPerUnitOfMeasure: 5,
        isArchived: false,
        isDeleted: false,
      });
    });

    it('should not change the skuId if already set', async () => {
      const appData = [{
        skuBatchId: '1',
        skuId: '3',
        wmsId: '1',
        quantityPerUnitOfMeasure: 10,
        isArchived: false,
        isDeleted: false,
      }];

      const inventoryData = [{
        skuBatchId: '1',
        skuId: '1',
        wmsId: '1',
        quantityPerUnitOfMeasure: 10,
        isArchived: false,
        isDeleted: false,
      }];

      const deltas: SkuBatchData[] = findDeltas(appData, inventoryData);
      expect(deltas.length).toBe(0);
    });

    it('should pick up change to skuId if not set', async () => {
      const appData = [{
        skuBatchId: '1',
        skuId: '1',
        wmsId: '1',
        quantityPerUnitOfMeasure: 10,
        isArchived: false,
        isDeleted: false,
      }];

      const inventoryData = [{
        skuBatchId: '1',
        skuId: null,
        wmsId: '1',
        quantityPerUnitOfMeasure: 10,
        isArchived: false,
        isDeleted: false,
      }];

      const deltas: SkuBatchData[] = findDeltas(appData, inventoryData);
      expect(deltas.length).toBe(1);
      expect(deltas[0]).toEqual({
        skuBatchId: '1',
        skuId: '1',
        wmsId: '1',
        quantityPerUnitOfMeasure: 10,
        isArchived: false,
        isDeleted: false,
      });
    });

    it('should pick up change to wmsId', async () => {
      const appData = [{
        skuBatchId: '1',
        skuId: '1',
        wmsId: '4',
        quantityPerUnitOfMeasure: 10,
        isArchived: false,
        isDeleted: false,
      }];

      const inventoryData = [{
        skuBatchId: '1',
        skuId: '1',
        wmsId: '1',
        quantityPerUnitOfMeasure: 10,
        isArchived: false,
        isDeleted: false,
      }];

      const deltas: SkuBatchData[] = findDeltas(appData, inventoryData);
      expect(deltas.length).toBe(1);
      expect(deltas[0]).toEqual({
        skuBatchId: '1',
        skuId: '1',
        wmsId: '4',
        quantityPerUnitOfMeasure: 10,
        isArchived: false,
        isDeleted: false,
      });
    });

    it('should find changes between datasets', async () => {
      await expect(
        findChangesBetweenDatasets(),
      ).resolves.toStrictEqual([{
        isDeleted: true,
        isArchived: false,
        quantityPerUnitOfMeasure: 1,
        skuBatchId: 'sku-batch-id-5',
        skuId: 'sku-id-2',
        wmsId: 1238,
      }, {
        isDeleted: false,
        isArchived: true,
        quantityPerUnitOfMeasure: 1,
        skuBatchId: 'sku-batch-id-6',
        skuId: 'sku-id-3',
        wmsId: 1239,
      }]);
    });
  });
});
