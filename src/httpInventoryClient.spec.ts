import { HttpInventoryClient } from './httpInventoryClient';
import { RecordWithWMS } from './interfaces.util';
import axios from 'axios';

jest.mock('axios');

describe('HTTP Inventory Client', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('.create', () => {
    // create will post to the inventory and inventory aggregate endpoints
    it('should post the record to both endpoints', async () => {
      const subject = new HttpInventoryClient();
      const record: RecordWithWMS = {
        skuBatchId: 'sku-batch-id-3',
        skuId: '142',
        wmsId: 1234,
        quantityPerUnitOfMeasure: 5,
        warehouseId: '2'
      }
      // @ts-ignore
      axios.post.mockResolvedValue({status: 200})

      await subject.create(record);

      // @ts-ignore
      const calls: any = axios.post.mock.calls;

      expect(axios.post).toHaveBeenCalledTimes(2);
      expect(calls[0][0]).toEqual('https://local-inventory.nabis.dev/v1/inventory');
      expect(calls[0][1]).toEqual(record);
      expect(calls[1][0]).toEqual('https://local-inventory.nabis.dev/v1/inventory-aggregate');
      expect(calls[1][1]).toEqual(record);
    });
  });

  describe('.update', () => {
    // update will call put on the inventory and inventory aggregate endpoints
    // put will send entire body of the item
    it('should put the record to both endpoints', async () => {
      const subject = new HttpInventoryClient();
      const record: RecordWithWMS = {
        skuBatchId: 'sku-batch-id-3',
        skuId: '142',
        wmsId: 1234,
        quantityPerUnitOfMeasure: 5,
        warehouseId: '2'
      }
      // @ts-ignore
      axios.put.mockResolvedValue({status: 200})

      await subject.update(record);

      // @ts-ignore
      const calls: any = axios.put.mock.calls;

      expect(axios.put).toHaveBeenCalledTimes(2);
      expect(calls[0][0]).toEqual('https://local-inventory.nabis.dev/v1/inventory');
      expect(calls[0][1]).toEqual(record);
      expect(calls[1][0]).toEqual('https://local-inventory.nabis.dev/v1/inventory-aggregate');
      expect(calls[1][1]).toEqual(record);
    });
  });
});